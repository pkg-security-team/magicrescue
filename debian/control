Source: magicrescue
Section: utils
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Joao Eriberto Mota Filho <eriberto@debian.org>
Build-Depends: debhelper-compat (= 13), libgdbm-dev, libgdbm-compat-dev
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://github.com/jbj/magicrescue
Vcs-Git: https://salsa.debian.org/pkg-security-team/magicrescue.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/magicrescue

Package: magicrescue
Architecture: any
Depends: dcraw,
         flac,
         libjpeg-turbo-progs,
         mpg123,
         sqlite3,
         unzip,
         zip,
         ${misc:Depends},
         ${shlibs:Depends}
Description: recover files by looking for magic bytes
 Magic Rescue scans a block device for file types it knows how to recover
 and calls an external program to extract them. It looks at "magic bytes"
 (file patterns) in file contents, so it can be used both as an undelete
 utility and for recovering a corrupted drive or partition. As long as
 the file data is there, it will find it.
 .
 Magic Rescue uses files called 'recipes'. These files have strings and
 commands to identify and extract data from devices or forensics images.
 So, you can write your own recipes. Currently, there are the following
 recipes: avi, canon-cr2, elf, flac, gpl, gzip, jpeg-exif, jpeg-jfif,
 mbox, mbox-mozilla-inbox, mbox-mozilla-sent, mp3-id3v1, mp3-id3v2,
 msoffice, nikon-raw, perl, png, ppm, sqlite and zip.
 .
 This package provides magicrescue, dupemap and magicsort commands.
 magicrescue is a carver and it is useful in forensics investigations.
